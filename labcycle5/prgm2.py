from shutil import ReadError


class Rectangle:
    def __init__(self, length, breadth):
        self.rectLength = length
        self.rectBreadth = breadth
        self.rectArea = 0
        self.rectPerimeter = 0

    def area(self):
        self.rectArea = self.rectLength * self.rectBreadth

    def perimeter(self):
        self.rectPerimeter = 2 * (self.rectLength + self.rectBreadth)

    def printData(self):
        print("Length : ", self.rectLength)
        print("Breadth : ", self.rectBreadth)
        print("Area : ", self.rectArea)
        print("Perimeter : ", self.rectPerimeter)


def takeLengthInput():
    length = int(input("Enter the length : "))
    return length


def takeBreadthInput():
    breadth = int(input("Enter the breadth : "))
    return breadth
    


def main():
    length1 = takeLengthInput()
    breadth1 = takeBreadthInput()
    obj1 = Rectangle(length1, breadth1)
    obj1.area()
    obj1.perimeter()
    obj1.printData()
    length2 = takeLengthInput()
    breadth2 = takeBreadthInput()
    obj2 = Rectangle(length2, breadth2)
    obj2.area()
    obj2.perimeter()
    obj2.printData()


if __name__ == "__main__":
    main()
