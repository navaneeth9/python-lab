class BankAccount:
    def __init__(self, name, accountNumber, accountType, balance):
        self.name = name
        self.accNo = accountNumber
        self.accType = accountType
        self.accbalance = balance

    def withdrawAmount(self, amount):
        if amount >= self.accbalance:
            print("Cannot withdraw this much amount as the balance is ",
                  self.accbalance)
            return

        print(amount, " Amount withdrawn from your account")
        self.accbalance -= amount
        print("Balance amount in your account :", self.accbalance)

    def printData(self):
        print("Name : " + self.name)
        print("Account Number : ", self.accNo)
        print("Account Type : " + self.accType)
        print("Balance Amount :", self.accbalance)


def main():
    name = input("Enter yout name : ")
    accountNumber = int(
        input("Enter the Account number(only integers allowed) : "))
    accountType = input(
        "Enter the type of account(savings / current / salary / FD(fidxed deposit) ) : ")
    balance = int(input("Enter the amount you want to deposit : "))

    account = BankAccount(name, accountNumber, accountType, balance)

    while True:
        account.printData()
        amount = int(input("Enter the amount you want to withdaraw : "))
        account.withdrawAmount(amount)
        choice = input("Do you want to continue(y/n) : ")
        if choice == 'n':
            return False
            


if __name__ == "__main__":
    main()
